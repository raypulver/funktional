var curry = funktional.curry,
    uncurryThis = funktional.uncurryThis,
    compose = funktional.compose,
    curryThis = funktional.curryThis;

function returnThis() { return this; }

describe('funktional utility modules', function () {
  it('should curry', function () {
    expect(curry(function (a, b) { return a + b; }, 5, 4)()).to.equal(9);
  });
  it('should curry multiple times', function () {
    expect(curry(curry(function (a, b) { return a + b }, 5), 6)()).to.equal(11);
  });
  it('should uncurry thisArg', function () {
    var results = [];
    var input = [1, 2];
    var forEach = uncurryThis([].forEach);
    forEach(input, function (v) { results.push(v); });
    expect(results).to.eql([1, 2]);
  });
  it('should curry this arg', function () {
    var results = [];
    var forEach = curryThis([].forEach, [5, 6]);
    forEach(function (v) {
      results.push(v);
    });
    expect(results).to.eql([5, 6]);
  });
  it('should compose unary functions', function () {
    expect(compose(compose(function (a) { return a*2; }, function (a) { return a*3; }), function (a) { return a*4; })(5)).to.equal(120);
  });
});
