"use strict";

export default (fn, ...args) => function (...restArgs) { return fn.apply(this, args.concat(restArgs)); };
