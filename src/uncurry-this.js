"use strict";

export default (fn) => (thisArg, ...args) => fn.apply(thisArg, args);
