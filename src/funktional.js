"use strict";

import curry from './curry';
import curryThis from './curry-this';
import uncurryThis from './uncurry-this';
import compose from './compose';

export default {
  curry,
  curryThis,
  uncurryThis,
  compose
};
