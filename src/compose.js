"use strict";

import reduce from 'array-reduce';

export default (...fns) => function (arg) {
  return reduce(fns, (r, v) => v.call(this, r), arg);
};
