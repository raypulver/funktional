(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("compose", [], factory);
	else if(typeof exports === 'object')
		exports["compose"] = factory();
	else
		root["compose"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _arrayReduce = __webpack_require__(1);
	
	var _arrayReduce2 = _interopRequireDefault(_arrayReduce);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function () {
	  for (var _len = arguments.length, fns = Array(_len), _key = 0; _key < _len; _key++) {
	    fns[_key] = arguments[_key];
	  }
	
	  return function (arg) {
	    var _this = this;
	
	    return (0, _arrayReduce2.default)(fns, function (r, v) {
	      return v.call(_this, r);
	    }, arg);
	  };
	};
	
	module.exports = exports["default"];

/***/ },
/* 1 */
/***/ function(module, exports) {

	var hasOwn = Object.prototype.hasOwnProperty;
	
	module.exports = function (xs, f, acc) {
	    var hasAcc = arguments.length >= 3;
	    if (hasAcc && xs.reduce) return xs.reduce(f, acc);
	    if (xs.reduce) return xs.reduce(f);
	    
	    for (var i = 0; i < xs.length; i++) {
	        if (!hasOwn.call(xs, i)) continue;
	        if (!hasAcc) {
	            acc = xs[i];
	            hasAcc = true;
	            continue;
	        }
	        acc = f(acc, xs[i], i);
	    }
	    return acc;
	};


/***/ }
/******/ ])
});
;
//# sourceMappingURL=compose.js.map