"use strict";

var path = require('path'),
    deepAssign = require('deep-assign'),
    join = path.join,
    clone = require('clone'),
    parse = path.parse,
    sep = require('os').sep,
    spawnSync = require('child_process').spawnSync,
    fs = require('fs'),
    writeFileSync = fs.writeFileSync,
    readdirSync = fs.readdirSync,
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    mapSeries = require('async').mapSeries,
    webpack = require('webpack'),
    ClosureCompilerPlugin = require('webpack-closure-compiler'),
    mocha = require('gulp-mocha'),
    babel = require('gulp-babel');

var jshintConfig = {};
require.extensions['.json'](jshintConfig, './.jshintrc');
jshintConfig = jshintConfig.exports;

var keys = Object.keys,
    isArray = Array.isArray,
    task = gulp.task.bind(gulp),
    JSONstringify = JSON.stringify,
    tasks = gulp.tasks;

var packageJson = require(join(__dirname, 'package'));

var minifierPlugin = new ClosureCompilerPlugin({
  compiler: {
    language_in: 'ECMASCRIPT6',
    language_out: 'ECMASCRIPT5'
  },
  concurrency: 3
});

var chunks = readdirSync(join(__dirname, 'src')).filter(isJSFile).map(filenameBase);

var baseWebpackCfg = {
  output: {
    path: __dirname,
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  devtool: 'source-map',
  module: {
    loaders: [{
      test: /(?:\.js$)/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'stage-2'],
        plugins: ['add-module-exports']///, 'transform-es3-member-expression-literals', 'transform-es3-property-literals']
      }
    }]
  }
};

function makeWebpackCfg(chunkName) {
  return deepAssign({
    entry: appendDirnameSrc(chunkName),
    output: {
      filename: appendJSExt(chunkName),
      library: camelize(chunkName)
    }
  }, baseWebpackCfg);
}

function makeWebpackMinCfg(chunkName) {
  return deepAssign(makeWebpackCfg(chunkName), {
    output: {
      filename: appendMinJSExt(chunkName)
    },
    plugins: [minifierPlugin]
  });
}
  

function prettyStringify(v) {
  return JSONstringify(v, null, 1);
}

function forOwn(o, cb) {
  keys(o).forEach(function (v) {
    cb(o[v], v, o);
  });
}

function binPath(exe) {
  return join('node_modules', '.bin', exe);
}

function nodeTask(exe) {
  return 'node ' + binPath(exe);
}

function nodeTaskArgs(exe, args) {
  if (!isArray(args)) args = [args];
  return nodeTask(exe) + ' ' + args.join(' ');
}

var gulpTask = nodeTaskArgs.bind(null, 'gulp');

function appendSep(str) {
  return String(str) + sep;
}

function doWebpack(cfg, next) {
  webpack(cfg, function (err, stats) {
    if (err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString({
      colors: true,
      chunkModules: true
    }));
    next();
  });
}

task('build:tasks', function () {
  packageJson.scripts = {};
  forOwn(tasks, function (v, k) {
    packageJson.scripts[k] = gulpTask(k);
  });
  writeFileSync('package.json', prettyStringify(packageJson));
});

task('build', ['webpack:dev', 'webpack:min']);

task('webpack:dev', function (next) {
  mapSeries(chunks, function (v, next) {
    doWebpack(makeWebpackCfg(v), next);
  }, function (err, results) {
    if (err) gutil.log(new gutil.PluginError('webpack', err).stack);
    next();
  });
});

task('webpack:min', function (next) {
  mapSeries(chunks, function (v, next) {
    doWebpack(makeWebpackMinCfg(v), next);
  }, function (err, results) {
    if (err) gutil.log(new gutil.PluginError('webpack', err).stack);
    next();
  });
});

task('jshint', function () {
  return gulp.src('./src/**/*.js')
    .pipe(jshint(jshintConfig))
    .pipe(jshint.reporter('jshint-stylish'));
});

task('test', function () {
  return gulp.src('test/test.js', { read: false })
    .pipe(mocha({ reporter: 'nyan' }));
});

task('karma', function () {
  spawnSync(binPath('karma'), ['start', 'karma.conf.js'], { stdio: 'inherit' });
});

function camelize(s) {
  return s.split('-').map(function (v, i) {
    return i && v.substr(0, 1).toUpperCase() + v.substr(1) || v;
  }).join('');
}

function isJSFile(s) {
  return parse(s).ext === '.js';
}

function filenameBase(s) {
  return parse(s).name;
}

function appendJSExt(s) {
  return s + '.js';
}

function appendMinJSExt(s) {
  return s + '.min.js';
}

function appendDirnameSrc(s) {
  return join(__dirname, 'src', s);
}
